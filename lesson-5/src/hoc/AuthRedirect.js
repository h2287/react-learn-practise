import React from 'react';
import { connect } from 'react-redux';
import {Navigate, useNavigate} from "react-router-dom";

let mapStateToPropsForRedirect = (state) => ({
  isAuth: state.auth.isAuth,
});

export const withNavigate = (Component) => {
    let RedirectTo =(props) => {
        return < Component {...props} navigate={useNavigate() } />
    }
    return RedirectTo;
}

export const withAuthRedirect = (Component) => {
    let redirectComponent = (props) =>  {
        if (!props.isAuth) return <Navigate to={'/login'} />
        return <Component {...props} />
    }
    
    let ConnectedAuthRedirectComponent = connect(mapStateToPropsForRedirect)(redirectComponent);

    return ConnectedAuthRedirectComponent;
}