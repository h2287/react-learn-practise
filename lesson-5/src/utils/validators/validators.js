export const required=values=>{
   if (values) return undefined;
  return "Field is required";
}

export const maxLengthCreator=(maxLeength)=>(values)=>{
  if (values.length >maxLeength) return `Max length is ${maxLeength} symbols`;
 return undefined;
}