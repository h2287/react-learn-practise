import React from "react";
import preloader from "./../../../assets/loading/Filled fading balls.gif";

let Preloader =(props)=>{
  return <div style={{ backgroundColor: 'white' }}><img src={preloader} /></div>
}

export default Preloader;