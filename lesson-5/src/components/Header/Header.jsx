import React from "react";
import { NavLink } from "react-router-dom";
import s from './css/Header.module.css';

const Header = (props) => {
  return <header className={s.header}>
    <img src='https://i.pinimg.com/originals/0d/cf/b5/0dcfb548989afdf22afff75e2a46a508.jpg'></img>

    <div className={s.loginBlock}>
      {props.isAuth 
      ? <div> {props.login} - <button onClick={props.logout}>Log out</button></div>
        : <NavLink to={'/login'}>Login</NavLink>}
    </div>
  </header>
}

export default Header;