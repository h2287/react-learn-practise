import React from "react";
import { Navigate } from "react-router-dom";
import { Field, reduxForm } from "redux-form";
import { Teaxtarea } from "../common/Preloader/FormsConstrols/FormsControls";
import s from "./css/Dialogs.module.css";
import DialogsItem from "./DialogItem/DialogItem";
import Message from "./Messege/Messege";
import { maxLengthCreator, required } from "../../utils/validators/validators";

const Dialogs = (props) => {
  let state = props.dialogsPage;

  let dialogsElements = state.dialogs.map((d) => (
    <DialogsItem name={d.name} id={d.id} key={d.id} />
  ));
  let messagesElements = state.message.map((m) => (
    <Message message={m.message} key={m.id} />
  ));
  // let newMessageBody = state.newMessageBody;

  let addNewMessage = (values) => {
    props.sendMessage(values.newMessageBody);
  };

  if (!props.isAuth) return <Navigate to="/login" />;

  return (
    <div className={s.dialogs}>
      <div className={s.dialogsItems}>{dialogsElements}</div>
      <div className={s.message}>
        <div>{messagesElements}</div>
      </div>
      <AddMessageFormRedux onSubmit={addNewMessage}/>
    </div>
  );
};

const maxLength50=maxLengthCreator(50);

const AddMessageForm = (props) => {
  return (
    <form onSubmit={props.handleSubmit}>
      <div>
        <Field component={Teaxtarea} validate={[required, maxLength50]} name="newMessageBody"
         placeholder="Enter your message"/>
      </div>
      <div>
        <button >Send</button>
      </div>
    </form>
  )
}

const AddMessageFormRedux=reduxForm({form: "dialogAddMessageForm"})(AddMessageForm)

export default Dialogs;
